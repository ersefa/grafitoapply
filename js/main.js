$(document).ready(function() {
	$('.pagewrapper').click(function() {
		$('.mark').css('background-color','transparent');
		$(this).find('.mark').css('background-color','white');
		$('.above').hide()
		$(this).find('.above').show();
	});

	$('#morepopup').click(function () {
		popupshow();
	});
});

function popupshow() {
	$('#popup').slideDown(600);
	$('#morepopup').removeClass('moreoff').addClass('moreon');
	
}

function popuphide() {
	$('#popup').slideUp(600);
	$('#morepopup').removeClass('moreon').addClass('moreoff');
}


/* BOTTOM ANIMATION */
function showbottom() {
	$('#content').animate({height: "455px"}, 600);
	$('#bottom').animate({height: "345px"}, 600);
	$('.bottombottomcontent').show();
	$('#togglebottom').attr('onclick', 'hidebottom()');
	$('#togglebottom').removeClass('moreoff').addClass('less');
}

function hidebottom() {
	$('#bottom').animate({height: "145px"}, 600, function() {
		$('.bottombottomcontent').hide();
	});
	$('#content').animate({height: "655px"}, 600);
	$('#togglebottom').attr('onclick', 'showbottom()');
	$('#togglebottom').removeClass('less').addClass('moreoff');
}

$(document).mouseup(function (e)
{
    var container = $("#popup");

    if (container.has(e.target).length === 0)
    {
        popuphide();
    }
});